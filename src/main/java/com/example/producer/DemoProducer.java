package com.example.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.example.config.AppConfig;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class DemoProducer extends GenericKafkaProducer<String, String> {

	private final AppConfig appConfig;

	@Autowired
	public DemoProducer(AppConfig appConfig,
			KafkaTemplate<String, String> kafkaTemplate) {
		super(kafkaTemplate);
		this.appConfig = appConfig;
	}

	@Override
	protected String getTopic() {
		return appConfig.getKafkaDemoTopic();
	}

	
	public void produceDemodMessage(String key, String message) {
		try {
			super.publishMessage(key, message);
		} catch (KafkaPublishingException e) {
			log.error("Exception while producing demo message.");
		}
	}
}
