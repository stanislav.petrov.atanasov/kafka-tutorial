package com.example.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.example.config.AppConfig;
import com.example.dto.avro.DemoTestDTO;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class DemoAvroProducer extends GenericKafkaProducer<String, DemoTestDTO> {

	private final AppConfig appConfig;

	@Autowired
	public DemoAvroProducer(AppConfig appConfig,
			KafkaTemplate<String, DemoTestDTO> kafkaTemplate) {
		super(kafkaTemplate);
		this.appConfig = appConfig;
	}

	@Override
	protected String getTopic() {
		return appConfig.getKafkaDemoAvroTopic();
	}

	
	public void produceDemodMessage(String key, DemoTestDTO message) {
		try {
			super.publishMessage(key, message);
		} catch (KafkaPublishingException e) {
			log.error("Exception while producing demo message.");
		}
	}
}
