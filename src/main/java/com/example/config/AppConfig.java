package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class AppConfig {

	private final Environment env;

	@Autowired
	public AppConfig(Environment env) {
		this.env = env;
	}
	public String getKafkaConsumerGroupName() {
		return env.getProperty("kafka.consumer.groupname");
	}
	
	public String getKafkaServer() {
		return env.getProperty("kafka.bootstrapserver");
	}
	
	public String getKafkaSchemaRegistryUrl() {
		return env.getProperty("kafka.registry");
	}

	public boolean getKafkaSslEnabled() {
		return Boolean.valueOf(env.getProperty("kafka.ssl.enabled"));
	}
	
	public String getKafkaKeyStoreLocation() {
		return env.getProperty("kafka.keystore_location");
	}
	
	public String getKafkaKeyStorePassword() {
		return env.getProperty("kafka.keystore_password");
	}
	
	public String getKafkaTrustStoreLocation() {
		return env.getProperty("kafka.truststore_location");
	}
	
	public String getKafkaTrustStorePassword() {
		return env.getProperty("kafka.truststore_password");
	}
	
	public static final String KAFKA_DEMO_TOPIC ="kafka.topics.demo" ;
	public static final String KAFKA_DEMO_TOPIC_NAME ="${"+KAFKA_DEMO_TOPIC+"}";
	public String getKafkaDemoTopic() {
		return env.getProperty(KAFKA_DEMO_TOPIC);
	}
	
	public static final String KAFKA_AVRO_DEMO_TOPIC ="kafka.topics.demo.avro" ;
	public static final String KAFKA_AVRO_DEMO_TOPIC_NAME ="${"+KAFKA_AVRO_DEMO_TOPIC+"}";
	public String getKafkaDemoAvroTopic() {
		return env.getProperty(KAFKA_AVRO_DEMO_TOPIC);
	}
}

