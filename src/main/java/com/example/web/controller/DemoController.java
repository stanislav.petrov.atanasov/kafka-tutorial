package com.example.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.dto.avro.DemoTestDTO;
import com.example.producer.DemoAvroProducer;
import com.example.producer.DemoProducer;


@Controller
public class DemoController {

	private DemoProducer producer;
	private DemoAvroProducer producerAvro;
	@Autowired
	public DemoController(DemoProducer producer, DemoAvroProducer producerAvro) {
		super();
		this.producer = producer;
		this.producerAvro = producerAvro;
	}

	@RequestMapping("/")
	@ResponseBody
	String home(){
		return "Hello";
	}
	
	@RequestMapping("/publish")
	@ResponseBody
	String publish(){
		
		//The key is an optional message key that was used for partition assignment. The key can be null.
		String key = "334";
		//The value is the actual message contents as an opaque byte array. Kafka supports recursive messages in which case this may itself contain a message set.
		String value = "Hello world!";
	   producer.produceDemodMessage(key, value);
		
		return value;
	}
	@RequestMapping("/publishAvro")
	@ResponseBody
	String publishAvro(){
		
		DemoTestDTO dto = new DemoTestDTO();
		dto.setId(45L);
		dto.setInfo("Avro info");

		producerAvro.produceDemodMessage(dto.getId().toString(), dto);
		
		return dto.getInfo().toString();
	}
}