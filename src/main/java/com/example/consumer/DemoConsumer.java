package com.example.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.config.AppConfig;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DemoConsumer {

	@KafkaListener(topics = AppConfig.KAFKA_DEMO_TOPIC_NAME, containerFactory = GenericKafkaConsumerConfig.factoryName)
	public void receive(String message) {
		log.info("Our message is :" + message);
	}
	
}
