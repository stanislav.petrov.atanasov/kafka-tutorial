package com.example.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.config.AppConfig;
import com.example.dto.avro.DemoTestDTO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DemoAvroConsumer {

	@KafkaListener(topics = AppConfig.KAFKA_AVRO_DEMO_TOPIC_NAME, containerFactory = GenericKafkaConsumerConfig.factoryName)
	public void receive(DemoTestDTO message) {
		log.info("Our message is :" + message.toString());
	}
	
}
